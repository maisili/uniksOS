{
  description = "uniksOS";

  outputs = { self, mkUniksOS, mkHyraizyn, metastriz }@fleiks:
  {
    datom = let
      inherit (builtins) attrNames listToAttrs;

      mkMetaUniksOSNV = metastraGivenNeim:
      let
        metastra = fleiks.metastriz.datom.${metastraGivenNeim};
        astriNeimz = attrNames metastra.astriz;

        mkUniksOSFromAstriNeim = astraGivenNeim:
        let
          hyraizyn = mkHyraizyn.datom {
            inherit astraGivenNeim metastraGivenNeim;
          };
        in {
          name = astraGivenNeim;
          value = mkUniksOS.datom hyraizyn;
        };

        uniksOSNVz = map mkUniksOSFromAstriNeim astriNeimz;

        uniksOSIndeks = listToAttrs uniksOSNVz;

      in {
        name = metastraGivenNeim;
        value = uniksOSIndeks;
      };

      metastrizNeimz = attrNames metastriz.datom;

      metaUniksOSNV = map mkMetaUniksOSNV metastrizNeimz;

    in listToAttrs metaUniksOSNV;
  };
}
